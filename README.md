# Module htcacheclean

Ce module Drupal permet d'effectuer la purge des caches de l'extension Apache
`mod_cache` en même temps que les autres caches Drupal.

La configuration du module permet de spécifier le chemin de l'éxécutable
`htcacheclean` ainsi que le chemin du répertoire de stockage des fichiers de
cache `mod_cache`.
